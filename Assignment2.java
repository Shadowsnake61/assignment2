/*

Date: February 10, 2014
Programmer: Jonathan Kartes
Purpose: To ask the user for 5 numbers and then to give back the median, mode, max value, minimum value, sum, and average
*/

import java.util.Scanner;



public class Assignment2

{
	public static void main(String[] args)
	
	{
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println(" ");
		
		System.out.println("Enter Five Numbers:");		

		double min;
		double max;

		double one = keyboard.nextInt();

		min = one;
		max = one;

		double two = keyboard.nextInt();

		if (two < min) {
			min = two;
		}
		else if (two > max) {
			max = two;
		}

		double three = keyboard.nextInt();

		if (three < min) {
			min = three;
		}
		else if (three > max) { 
			max = three;
		}

		double four = keyboard.nextInt();

		if (four < min) {
			min = four;
		}
		else if (four > max) {
			max = four;
		}

		double five = keyboard.nextInt();
		
		if (five < min) {
			min = five;
		}
		else if (five > max) {
			max = five;
		}
		
		double average = (one + two + three + four + five) / 5;
		double sum = one + two + three + four + five;
		double median = (max + min) / 2;

		System.out.println(" ");		

		System.out.println("The median is: "+median);		
		System.out.println(" ");

		System.out.println("The maximum is: "+max);
		System.out.println(" ");

		System.out.println("The minimum is: "+min);
		System.out.println(" ");

		System.out.println("The sum is: "+sum);
		System.out.println(" ");

		System.out.println("The average is: "+average);
	}
} 	